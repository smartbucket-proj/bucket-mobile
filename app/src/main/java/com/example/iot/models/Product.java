package com.example.iot.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

@JsonObject
public class Product implements Parcelable {
    @JsonField
    private int id;
    @JsonField
    private String name;
    @JsonField
    private String description;
    @JsonField
    private float calorie;
    @JsonField
    private float price;
    @JsonField
    private String imageLink;

    public Product(String name, String description, float calorie, float cost, String imageURL) {
        this.name = name;
        this.description = description;
        this.calorie = calorie;
        this.price = cost;
        this.imageLink = imageURL;
    }

    public Product(Parcel in) {
        id = in.readInt();
        name = in.readString();
        description = in.readString();
        calorie = in.readFloat();
        price = in.readFloat();
        imageLink = in.readString();
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getCalorie() {
        return calorie;
    }

    public void setCalorie(float calorie) {
        this.calorie = calorie;
    }

    public float getCost() {
        return price;
    }

    public void setCost(float cost) {
        this.price = cost;
    }

    public String getImageURL() {
        return imageLink;
    }

    public void setImageURL(String imageURL) {
        this.imageLink = imageURL;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
        parcel.writeString(description);
        parcel.writeFloat(calorie);
        parcel.writeFloat(price);
        parcel.writeString(imageLink);
    }
}
