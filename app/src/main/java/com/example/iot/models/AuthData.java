package com.example.iot.models;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

@JsonObject
public class AuthData {
    @JsonField(name = "token")
    String token;
//    @JsonField(name = "BasketId")
//    int BasketId;

    public AuthData() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

//    public int getBasketId() {
//        return BasketId;
//    }
//
//    public void setBasketId(int basketId) {
//        this.BasketId = basketId;
//    }
}
