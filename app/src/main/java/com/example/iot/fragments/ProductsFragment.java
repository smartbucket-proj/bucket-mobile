package com.example.iot.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.iot.network.OperationCallback;
import com.example.iot.adapters.ProductListAdapter;
import com.example.iot.adapters.ProductListListener;
import com.example.iot.PurchaseActivity;
import com.example.iot.R;
import com.example.iot.databinding.FragmentProductListBinding;
import com.example.iot.models.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductsFragment extends BaseFragment implements ProductListListener {
    Boolean f = false;
    private FragmentProductListBinding binding;
    private PurchaseActivity parent;
    private ProductListAdapter adapter;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        parent = (PurchaseActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_product_list, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        parent.setcuurentFragment(this);
        initUI();
    }

    private void initUI() {
        LinearLayoutManager manager = new LinearLayoutManager(parent);
        binding.list.setLayoutManager(manager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(
                parent,
                manager.getOrientation());
        binding.list.addItemDecoration(dividerItemDecoration);
        adapter = new ProductListAdapter(new ArrayList<>(), this, getContext());
        binding.list.setAdapter(adapter);
        showProducts();
    }

    private void showProducts() {
        api.getProducts(new OperationCallback<List<Product>>() {
            @Override
            protected void onError(Exception error) {
                showToastOnUIThread("Нет соединение с сервером");
            }

            @Override
            protected void onCompleted(List<Product> result) {
                setList(result);
            }
        });
    }

    private void setList(List<Product> products) {
        adapter.setProducts(products);
    }

    @Override
    public void productOnClick(Product product) {
        parent.ProductDetails(product);
    }
}
