package com.example.iot.fragments;

import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.iot.network.IoTApi;

public class BaseFragment extends Fragment {
    public IoTApi api;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        api = new IoTApi(getContext());
    }

    public void showToastOnUIThread(final String message) {
        getActivity().runOnUiThread((() -> Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show()))
        ;
    }
}
