package com.example.iot.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.iot.network.IoTApi;
import com.example.iot.network.OperationCallback;
import com.example.iot.PurchaseActivity;
import com.example.iot.R;
import com.example.iot.adapters.AlternativeAdapter;
import com.example.iot.databinding.FragmentProductListBinding;
import com.example.iot.models.Product;

import java.util.ArrayList;
import java.util.List;

public class AlternativeProductFragment extends DialogFragment {
    private FragmentProductListBinding binding;
    private AlternativeAdapter adapter;
    private PurchaseActivity parent;

    public static AlternativeProductFragment getInstance(Product product) {
        AlternativeProductFragment fragment = new AlternativeProductFragment();
        Bundle args = new Bundle();
        args.putParcelable("product", product);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        parent = (PurchaseActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_product_list, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI();
    }

    private void initUI() {
        LinearLayoutManager manager = new LinearLayoutManager(parent);
        binding.list.setLayoutManager(manager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(
                parent,
                manager.getOrientation());
        binding.list.addItemDecoration(dividerItemDecoration);
        adapter = new AlternativeAdapter(new ArrayList<>(), getContext());
        binding.list.setAdapter(adapter);

        IoTApi api = new IoTApi(getContext());
        api.getProducts(new OperationCallback<List<Product>>() {
            @Override
            protected void onError(Exception error) {

            }

            @Override
            protected void onCompleted(List<Product> result) {
                setList(result);
            }
        });
    }

    private void setList(List<Product> products) {
        adapter.setProducts(products);
    }
}
