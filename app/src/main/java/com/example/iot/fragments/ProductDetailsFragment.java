package com.example.iot.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.example.iot.PurchaseActivity;
import com.example.iot.R;
import com.example.iot.databinding.FragmentProductDetailsBinding;
import com.example.iot.models.Product;
import com.squareup.picasso.Picasso;

public class ProductDetailsFragment extends BaseFragment {
    private FragmentProductDetailsBinding binding;
    private PurchaseActivity parent;

    public static ProductDetailsFragment getInstance(Product product) {
        ProductDetailsFragment fragment = new ProductDetailsFragment();
        Bundle args = new Bundle();
        args.putParcelable("product", product);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        parent = (PurchaseActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_product_details, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        parent.setcuurentFragment(this);
        initUI();
    }

    private void initUI() {
        Product product = getArguments().getParcelable("product");
        binding.productName.setText(product.getName());
        binding.productCalorie.setText(String.format("%s ккал", String.valueOf(product.getCalorie())));
        binding.productCost.setText(String.format("%s руб.", String.valueOf(product.getCost())));
        binding.productDescription.setText(product.getDescription());
        Picasso.get().load(product.getImageURL()).into(binding.productImage);
        binding.alternative.setOnClickListener(v -> showAlternative());
    }


    private void showAlternative() {
        AlternativeProductFragment mAddAccIdFragment = new AlternativeProductFragment();
        mAddAccIdFragment.show(parent.getSupportFragmentManager(), "addAccIdDialog");
    }
}
