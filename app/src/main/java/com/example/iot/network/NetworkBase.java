package com.example.iot.network;

import android.content.Context;

import com.readystatesoftware.chuck.ChuckInterceptor;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.internal.Util;

public class NetworkBase {
    protected final static short TYPE_GET = 0,
            TYPE_POST = 1,
            TYPE_PUT = 2,
            TYPE_DELETE = 3;
    private static final String CONTENT_TYPE = "application/json; charset=UTF-8",
            LOG_TAG = "Response";
    private final static MediaType MEDIA_TYPE_JSON = MediaType.parse(CONTENT_TYPE);
    private static String token;
    private static String uuid;
    public OkHttpClient client;

    public static String getUuid() {
        return uuid;
    }

    public static void setUuid(String uuid) {
        NetworkBase.uuid = uuid;
    }

    public void initClient(Context context) {
        client = new OkHttpClient.Builder()
                .addInterceptor(new ChuckInterceptor(context))
                .connectTimeout(5, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .build();
    }

    String getToken() {
        return NetworkBase.token;
    }

    public void setToken(String token) {
        NetworkBase.token = token;
    }

    Request.Builder createRequest(String path) {
        Request.Builder builder = new Request.Builder().url(path);
        if (token != null) builder.addHeader("Authorization", token);
        return builder;
    }

    Request.Builder createRequest(String path, short method) {
        Request.Builder builder = createRequest(path);
        switch (method) {
            case TYPE_GET:
                builder.get();
                break;
            case TYPE_POST:
//                builder.post()
        }
        return builder;
    }

    public void postRequset(String path, JSONObject params, Callback callback) {
        Request request = new Request.Builder()
                .url(path)
                .post(params != null ?
                        RequestBody.create(MEDIA_TYPE_JSON, params.toString()) :
                        Util.EMPTY_REQUEST)
                .build();
        client.newCall(request).enqueue(callback);

    }

    public static class ApiCallback implements okhttp3.Callback {
        protected Response response;
        OperationCallback callback;
        String responseString;

        ApiCallback() {
        }

        ApiCallback(OperationCallback callback) {
            this.callback = callback;
        }

        @Override
        public void onFailure(@NotNull Call call, @NotNull IOException e) {
            callback.notifyError(new Exception(e.getMessage()));
        }

        @Override
        public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
            this.response = response;
            if (response.body() != null && response.isSuccessful()) {
                responseString = response.body().string();
                onSuccess();
                return;
            }
            onError(response.code());
        }

        public void onSuccess() {
            callback.onCompleted(true);
        }

        public void onError(int code) {
            if (callback != null) callback.notifyError(new Exception(responseString));
        }
    }
}


