package com.example.iot.network;

import android.content.Context;
import android.provider.Settings;

import com.example.iot.models.AuthData;
import com.example.iot.models.Product;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import okhttp3.Call;

public class IoTApi extends NetworkBase {

    private static String server = "http://1b8e87d03290.ngrok.io";

    private Context context;

    public IoTApi(Context context) {
        this.context = context;
        initClient(context);
    }

//    public void getBasketToken(final OperationCallback<String> callback){
//        createRequest()
//    }

    public void Authorization(String basketNumber, OperationCallback<AuthData> callback) {
        setUuid(UUID.randomUUID().toString());

        client.newCall(createRequest(String.format("%s/mobile/connectToBasket?token=%s&mobile=%s", server,
                basketNumber, getDeviceId())).build()).
                enqueue(new NetworkBase.ApiCallback(callback) {
                    @Override
                    public void onSuccess() {
                        AuthData result;
                        result = new Gson().fromJson(responseString, AuthData.class);
                        setToken(result.getToken());
                        callback.notifyCompleted(result);
                    }

                    @Override
                    public void onFailure(@NotNull Call call, @NotNull IOException e) {
                        super.onFailure(call, e);
                    }
                });

    }

    public void getProducts(final OperationCallback<List<Product>> callback) {
        client.newCall(createRequest(String.format("%s/Product", server), TYPE_GET).build()).enqueue(new ApiCallback(callback) {
            @Override
            public void onSuccess() {
                String crutch = responseString.substring(responseString.indexOf('['), responseString.lastIndexOf(']') + 1);
                List<Product> result = new ArrayList<>();
                Type listType = new TypeToken<List<Product>>() {
                }.getType();
                result = new Gson().fromJson(crutch, listType);
                callback.notifyCompleted(result);
            }

            @Override
            public void onError(int code) {
                super.onError(code);
            }
        });
    }

    public String getDeviceId() {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

}
