package com.example.iot.network;

import android.os.Looper;

public abstract class OperationCallback<T> extends OperationCallbackBase {
    private DispatchType dispatchType;
    private T result;
    private T progress;
    private Exception error;

    public OperationCallback() {
        dispatchType = Looper.myLooper() == Looper.getMainLooper()
                ? DispatchType.MainThread : DispatchType.CurrentThread;
    }

    protected void onStart() {
    }

    protected abstract void onError(Exception error);

    protected abstract void onCompleted(T result);

    protected void onFinish() {
    }

    public void notifyProgress(T progress) {
        this.progress = progress;
        run();
    }

    public void notifyCompleted(T result) {
        this.result = result;
        dispatch();
    }

    public void notifyError(Exception error) {
        this.error = error;
        dispatch();
    }

    public OperationCallback<T> start() {
        onStart();
        return this;
    }

    public void run() {
        try {
            if (this.error != null) {
                onError(this.error);
                onFinish();
            } else if (this.result != null) {
                onCompleted(this.result);
                onFinish();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void dispatch() {
        try {
            if (dispatchType == DispatchType.MainThread) {
                DispatchToMainThread();
            } else if (dispatchType == DispatchType.NewThread) {
                DispatchToNewThread();
            } else {
                run();
            }
        } catch (Exception ignored) {
            String s = ignored.toString();
        }
    }
}
