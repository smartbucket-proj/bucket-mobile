package com.example.iot;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import androidx.databinding.DataBindingUtil;

import com.example.iot.databinding.ActivityMainBinding;
import com.example.iot.models.AuthData;
import com.example.iot.network.OperationCallback;

public class MainActivity extends BaseAsctivity {

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.inputCodeTV.requestFocus();
        setListener();
    }

    private void setListener() {
        binding.enterAppBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Auth();
            }
        });
        binding.inputCodeTV.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    //do what you want on the press of 'done'
                    Auth();
                }
                return false;
            }
        });
    }

    private void progressVisible(Boolean visible) {
        if (visible) {
            binding.enterAppBut.setText("");
            binding.progress.setVisibility(View.VISIBLE);
        } else {
            binding.enterAppBut.setText(R.string.enter);
            binding.progress.setVisibility(View.GONE);

        }
    }

    private void Auth() {
        progressVisible(true);
        api.Authorization(binding.inputCodeTV.getText().toString(), new OperationCallback<AuthData>() {

            @Override
            protected void onStart() {
            }

            @Override
            protected void onError(Exception error) {
                showToastOnUIThread("Введеный код неправильный");
                progressVisible(false);
            }

            @Override
            protected void onCompleted(AuthData result) {
                progressVisible(false);
                startActivity();
            }
        }.start());
    }

    private void startActivity() {
        Intent intent = new Intent(MainActivity.this, PurchaseActivity.class);

        startActivity(intent);

        finish();
    }
}
