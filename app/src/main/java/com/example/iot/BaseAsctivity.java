package com.example.iot;

import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.iot.network.IoTApi;

public class BaseAsctivity extends AppCompatActivity {
    public IoTApi api;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        api = new IoTApi(this);
        super.onCreate(savedInstanceState);
    }

    public void showToastOnUIThread(final String message) {
        runOnUiThread((() -> Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show()))
        ;
    }
}
