package com.example.iot;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentTransaction;

import com.example.iot.databinding.ActivityPurchaseBinding;
import com.example.iot.fragments.AlternativeProductFragment;
import com.example.iot.fragments.BaseFragment;
import com.example.iot.fragments.ProductDetailsFragment;
import com.example.iot.fragments.ProductsFragment;
import com.example.iot.models.Product;
import com.example.iot.network.NetworkBase;

public class PurchaseActivity extends BaseAsctivity {

    public static final String TYPE_PRODUCT = "products",
            TYPE_ALTERNATIVE = "alternative",
            TYPE_DETAILS = "details";
    ActivityPurchaseBinding binding;
    BaseFragment currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_purchase);
        initUI();
    }

    private void initUI() {
        setFragment(TYPE_PRODUCT);
    }

    public void ProductDetails(Product product) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        ProductDetailsFragment productDetailsFragment = ProductDetailsFragment.getInstance(product);
        transaction
                .replace(R.id.fragmentContainer, productDetailsFragment, TYPE_DETAILS)
                .addToBackStack(null)
                .commit();
    }

    public void setFragment(String type) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        switch (type) {
            case TYPE_PRODUCT:
                transaction.replace(R.id.fragmentContainer, new ProductsFragment(), TYPE_PRODUCT);
                break;
            case TYPE_ALTERNATIVE:
                transaction.replace(R.id.fragmentContainer, new AlternativeProductFragment(), TYPE_ALTERNATIVE);
                break;
        }
        transaction.commit();
    }

    public void setcuurentFragment(BaseFragment fragment) {
        currentFragment = fragment;
        switch (fragment.getTag()) {
            case TYPE_PRODUCT:
                binding.toolbarTitle.titleTV.setText("Список продуктов");
                break;
            case TYPE_ALTERNATIVE:
                binding.toolbarTitle.titleTV.setText("Альтернативный продукт");
            case  TYPE_DETAILS:
                binding.toolbarTitle.titleTV.setText("Подробно о продукте");
        }
    }

    @Override
    public void onBackPressed() {
        if (currentFragment instanceof ProductsFragment) {
            Intent intent = new Intent(PurchaseActivity.this, MainActivity.class);
            api.setToken(null);
            NetworkBase.setUuid(null);
            startActivity(intent);
            finish();
        } else {
            super.onBackPressed();
        }
    }

    public void back(View v) {
        onBackPressed();
    }
}
