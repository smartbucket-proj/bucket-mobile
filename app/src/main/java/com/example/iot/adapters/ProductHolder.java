package com.example.iot.adapters;

import android.content.Context;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.example.iot.R;
import com.example.iot.databinding.ProductItemViewBinding;
import com.example.iot.models.Product;
import com.squareup.picasso.Picasso;

import java.util.Random;

public class ProductHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private Product product;
    private ProductListListener listener;
    private ProductItemViewBinding binding;
    private Context context;

    public ProductHolder(ProductItemViewBinding binding, ProductListListener listener, Context context) {
        super(binding.getRoot());
        this.listener = listener;
        this.binding = binding;
        this.context = context;
        binding.getRoot().setOnClickListener(this::onClick);
    }

    public void setItem(Product product) {
        this.product = product;
        binding.name.setText(product.getName());
        binding.cal.setText(String.valueOf(product.getCalorie()));
        binding.cost.setText(String.valueOf(product.getCost()));
        if (new Random().nextBoolean()) {
            binding.getRoot().setBackgroundColor(context.getResources().getColor(R.color.product_background_bad));
        } else {
            binding.getRoot().setBackgroundColor(context.getResources().getColor(R.color.product_background_good));

        }
        Picasso.get().load(product.getImageURL()).into(binding.imageView);
    }

    @Override
    public void onClick(View v) {
        listener.productOnClick(product);
    }
}
