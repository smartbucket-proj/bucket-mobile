package com.example.iot.adapters;

import com.example.iot.models.Product;

public interface ProductListListener {
    void productOnClick(Product product);
}
