package com.example.iot.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.iot.R;
import com.example.iot.databinding.ProductItemViewBinding;
import com.example.iot.models.Product;

import java.util.List;

public class ProductListAdapter extends RecyclerView.Adapter {

    private List<Product> products;
    private ProductListListener listener;
    private Context context;

    public ProductListAdapter(List<Product> products, ProductListListener listener, Context context) {
        this.products = products;
        this.listener = listener;
        this.context = context;
    }

    public void setProducts(List<Product> products) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new PolicyDiffCallback(this.products, products));
        this.products.clear();
        this.products.addAll(products);
        diffResult.dispatchUpdatesTo(this);
//        this.products = products;
//        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ProductHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ProductItemViewBinding cellPolicyBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.product_item_view,
                parent,
                false);

        return new ProductHolder(cellPolicyBinding, listener, context);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((ProductHolder) holder).setItem(products.get(position));
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    private class PolicyDiffCallback extends DiffUtil.Callback {

        private List<Product> oldPolicies;
        private List<Product> newPolicies;

        public PolicyDiffCallback(List<Product> oldPolicies, List<Product> newPolicies) {
            this.newPolicies = newPolicies;
            this.oldPolicies = oldPolicies;
        }

        @Override
        public int getOldListSize() {
            return oldPolicies != null ? oldPolicies.size() : 0;
        }

        @Override
        public int getNewListSize() {
            return newPolicies != null ? newPolicies.size() : 0;
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            return oldPolicies.get(oldItemPosition).getId() == (newPolicies.get(newItemPosition).getId());
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            return oldPolicies.get(oldItemPosition).equals(newPolicies.get(newItemPosition));
        }
    }

}
