package com.example.iot.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.iot.R;
import com.example.iot.databinding.ProductItemViewBinding;
import com.example.iot.models.Product;

import java.util.List;

public class AlternativeAdapter extends RecyclerView.Adapter {

    private List<Product> products;
    private Context context;

    public AlternativeAdapter(List<Product> products, Context context) {
        this.products = products;
        this.context = context;
    }

    public void setProducts(List<Product> products) {
//        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new ProductListAdapter.PolicyDiffCallback(this.products, products));
//        this.products.clear();
//        this.products.addAll(products);
//        diffResult.dispatchUpdatesTo(this);
        this.products = products;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AlternativeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ProductItemViewBinding cellPolicyBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.product_item_view,
                parent,
                false);

        return new AlternativeHolder(cellPolicyBinding, context);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((AlternativeHolder) holder).setItem(products.get(position));
    }

    @Override
    public int getItemCount() {
        return products.size();
    }
}
