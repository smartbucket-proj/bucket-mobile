package com.example.iot.adapters;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

import com.example.iot.databinding.ProductItemViewBinding;
import com.example.iot.models.Product;
import com.squareup.picasso.Picasso;

public class AlternativeHolder extends RecyclerView.ViewHolder {

    private Product product;
    private ProductItemViewBinding binding;
    private Context context;

    public AlternativeHolder(ProductItemViewBinding binding, Context context) {
        super(binding.getRoot());
        this.binding = binding;
        this.context = context;
    }

    public void setItem(Product product) {
        this.product = product;
        binding.name.setText(product.getName());
        binding.cal.setText(String.valueOf(product.getCalorie()));
        binding.cost.setText(String.valueOf(product.getCost()));
        Picasso.get().load(product.getImageURL()).into(binding.imageView);
    }

}
